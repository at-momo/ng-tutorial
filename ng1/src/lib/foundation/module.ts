
namespace foundation {
    export class Module {
        public static instance: Module = null;
        protected module: ng.IModule;

        constructor(module: ng.IModule) {
            this.module = module;
            this.registComponent();
        }

        public static startup(): void {
            Module.getInstance();
        }

        public static getInstance(): Module {
            if (Module.instance == null) {
                Module.instance = Module.newInstance();
            }
            return Module.instance;
        }

        public static newInstance(): Module {
            // ToDo: 継承クラスで実装
            return null;
        }

        protected registComponent():void {
            // ToDo: 継承クラスで実装
        }

    }
}
