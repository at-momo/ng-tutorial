/// <reference path="../../references.d.ts" />

namespace foundation {

    export class Component {

        private module: ng.IModule = null;

        constructor(module: ng.IModule) {
            this.module = module;
            this.registController();
            this.registRouter();
        }

        protected registRouter(): void {
            // ToDo:継承クラスにて実装
        }

        protected registController(): void {
            // ToDo:継承クラスにて実装
        }

        protected addRouter(stateId: string, config: any): void {
            this.module.config([
                '$stateProvider',
                function ($stateProvider: ng.ui.IStateProvider) {
                    $stateProvider.state(
                        stateId, config
                    )
                }
            ]);
        }

    }

}
