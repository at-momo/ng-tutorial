/**
 * 3rd party library
 **/
/// <reference path="../typings/tsd.d.ts" />

/**
 * Foundation
 **/
/// <reference path="./lib/foundation/module.ts" />
/// <reference path="./lib/foundation/component.ts" />
/// <reference path="./lib/foundation/controller.ts" />
/// <reference path="./lib/foundation/service.ts" />


/// <reference path="./module/api/api.module.ts" />
/// <reference path="./module/api/service/yoyakuList.service.ts" />
/// <reference path="./module/api/vo/itemVo.ts" />
/// <reference path="./module/api/vo/yoyakuListVo.ts" />


/**
 * Appliaction
 **/
/// <reference path="./app/common/common.component.ts" />
/// <reference path="./app/appointment/appointment.component.ts" />
/// <reference path="./app/common/menu/controller/top.controller.ts" />
/// <reference path="./app/common/menu/controller/appointment.controller.ts" />
/// <reference path="./app/appointment/clinic/controller/top.controller.ts" />

