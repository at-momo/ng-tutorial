
namespace ivet.api {

    export class ApiModule {

        public static instance: ApiModule = null;
        private module: ng.IModule;
        static MODULE_ID: string = "ivet.api";

        constructor() {
            console.log("Regist Api Module!")
            this.module = angular.module(ApiModule.MODULE_ID, ['ngResource']);
            this.registService();
        }

        protected registComponent() {

        }

        protected registService() {
            console.log("Regist Service!")
            this.module.service(service.YoyakuListService.SERVICE_ID, service.YoyakuListService);
        }

        public static newInstance(): ApiModule {
            return new ApiModule();
        }

        public static startup(): void {
            ApiModule.getInstance();
        }

        public static getInstance(): ApiModule {
            if (ApiModule.instance == null) {
                ApiModule.instance = ApiModule.newInstance();
            }
            return ApiModule.instance;
        }


    }
}
