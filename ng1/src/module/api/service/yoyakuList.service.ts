/// <reference path="../../../references.d.ts" />

namespace ivet.api.service {

  export class YoyakuListService implements foundation.Service {

    public static SERVICE_ID: string = "ivet.api.yoyaku_list_service";
    public static END_POINT: string = "http://218.219.157.229/yoyakuList";

    private yoyakuList: vo.YoyakuListVo;

    constructor(
      private $resource: ng.resource.IResourceService,
      private $q: ng.IQService
    ) { }

    private load(): ng.IPromise<vo.YoyakuListVo> {
      let parent = this;
      let def = this.$q.defer();
      if (this.yoyakuList != null) {
        def.resolve(this.yoyakuList);
      } else {
        let resoruce = this.$resource(
          YoyakuListService.END_POINT,
          { callback: 'JSON_CALLBACK' }, { get: { method: 'JSONP' } }
        );
        resoruce.get({ tgt: 'Qry' }).$promise.then(
          function (response) {
            parent.yoyakuList = response;
            def.resolve(parent.yoyakuList);
          }
        );
      }
      return def.promise;
    }


    public getKarteKubunList(): ng.IPromise<vo.ItemVo[]> {
      let def = this.$q.defer();
      this.load().then(
        function (response) {
          def.resolve(response.karteKubunList);
        }
      );
      return def.promise;
    }


    public getJhokyoList(): ng.IPromise<vo.ItemVo[]> {
      let def = this.$q.defer();
      this.load().then(
        function (response) {
          def.resolve(response.jhokyoList);
        }
      );
      return def.promise;
    }


    public getDobutsushuList(): ng.IPromise<vo.ItemVo[]> {
      let def = this.$q.defer();
      this.load().then(
        function (response) {
          def.resolve(response.dobutsushuList);
        }
      );
      return def.promise;
    }


    public getShinryoukaList(): ng.IPromise<vo.ItemVo[]> {
      let def = this.$q.defer();
      this.load().then(
        function (response) {
          console.log(response.shinryoukaList);
          def.resolve(response.shinryoukaList);
        }
      );
      return def.promise;
    }


    public getTantoiList(): ng.IPromise<vo.ItemVo[]> {
      let def = this.$q.defer();
      this.load().then(
        function (response) {
          def.resolve(response.tantoiList);
        }
      );
      return def.promise;
    }

    public getShujiiList(): ng.IPromise<vo.ItemVo[]> {
      let def = this.$q.defer();
      this.load().then(
        function (response) {
          def.resolve(response.shujiiList);
        }
      );
      return def.promise;
    }
  }
}
