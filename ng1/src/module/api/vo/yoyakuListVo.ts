namespace ivet.api.vo {

    export class YoyakuListVo {
        jhokyoList: ItemVo;
        karteKubunList: ItemVo;
        dobutsushuList: vo.ItemVo;
        shinryoukaList: ItemVo;
        shujiiList: ItemVo;
        tantoiList: ItemVo;
    }

}