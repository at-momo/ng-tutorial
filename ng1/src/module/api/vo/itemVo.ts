namespace ivet.api.vo {

    export class ItemVo {
        id: number;
        name: string;
    }
}