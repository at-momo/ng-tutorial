/// <reference path="../../references.d.ts" />

namespace ivet.component {
    
    export class AppointmemtComponent extends foundation.Component {

        static COMPONENT_ID: string = "ivet.appointment";

        protected registRouter(): void {

            this.addRouter(
                ivet.appointment.clinic.controller.TopController.STATE_ID,
                ivet.appointment.clinic.controller.TopController.state
            );

        }

    }
}
