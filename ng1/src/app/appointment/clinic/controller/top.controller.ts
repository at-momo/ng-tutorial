

namespace ivet.appointment.clinic.controller {

    export class TopController {

        static CONTROLLER_ID: string = "ivet.appointment.clinic.top_controller";
        static STATE_ID: string = "appointment.clinic";
        static URL: string = "clinic/";
        static TEMPLATE_URL: string = "./assets/view/appointment/clinic/top.html";

        public appointmentGridOptions: any = {
            enableRowSelection: true,
            enableRowHeaderSelection: false,
            selectedItems: this.selectedKarteKubunList,
            multiSelect: true,
            columnDefs: [
                {field: 'id', displayName: 'id', width: '100', visible: false},//「visible:false 」で表示されなくなる
                {field: 'name', displayName: '予約日', width: '148'},
                {field: 'name', displayName: '時刻', width: '148'},
                {field: 'name', displayName: '飼主番号', width: '148'},
                {field: 'name', displayName: '動物番号', width: '148'},
                {field: 'name', displayName: '動物名', width: '148'},
            ]
        };


        public selectedKarteKubunList: api.vo.ItemVo[] = [];
        public karteKubunGridOptions: any = {
            enableRowSelection: true,
            enableRowHeaderSelection: false,
            selectedItems: this.selectedKarteKubunList,
            multiSelect: true,
            columnDefs: [
                { field: 'id', displayName: 'id', width: '100', visible: false },
                { field: 'name', displayName: "カルテ区分", width: '148' }
            ]
        };

        public selectedJhokyoList: api.vo.ItemVo[] = [];
        public jhokyoGridOptions: any = {
            enableRowSelection: true,
            enableRowHeaderSelection: false,
            selectedItems: this.selectedJhokyoList,
            multiSelect: true,
            columnDefs: [
                { field: 'id', displayName: 'id', width: '100', visible: false },
                { field: 'name', displayName: "状況", width: '148' }
            ]
        };


        public selectedDobutsushuList: api.vo.ItemVo[] = [];
        public dobutsushuGridOptions: any = {
            enableRowSelection: true,
            enableRowHeaderSelection: false,
            selectedItems: this.selectedDobutsushuList,
            multiSelect: true,
            columnDefs: [
                { field: 'id', displayName: 'id', width: '100', visible: false },
                { field: 'name', displayName: "動物種", width: '148' }
            ]
        };

        public selectedShinryokaList: api.vo.ItemVo[] = [];
        public shinryokaGridOptions: any = {
            enableRowSelection: true,
            enableRowHeaderSelection: false,
            selectedItems: this.selectedShinryokaList,
            multiSelect: true,
            columnDefs: [
                { field: 'id', displayName: 'id', width: '100', visible: false },
                { field: 'name', displayName: "診療科", width: '148' }
            ]
        };


        public selectedTantoiList: api.vo.ItemVo[] = [];
        public tantoiGridOptions: any = {
            enableRowSelection: true,
            enableRowHeaderSelection: false,
            selectedItems: this.selectedTantoiList,
            multiSelect: true,
            columnDefs: [
                { field: 'id', displayName: 'id', width: '100', visible: false },
                { field: 'name', displayName: "担当医", width: '148' }
            ]
        };

        public selectedShujiiList: api.vo.ItemVo[] = [];
        public shujiiGridOptions: any = {
            enableRowSelection: true,
            enableRowHeaderSelection: false,
            selectedItems: this.selectedShujiiList,
            multiSelect: true,
            columnDefs: [
                { field: 'id', displayName: 'id', width: '100', visible: false },
                { field: 'name', displayName: "担当医", width: '148' }
            ]
        };


        public static $inject = [
            '$scope',
            api.service.YoyakuListService.SERVICE_ID
        ];

        constructor(
            private $scope: ng.IScope,
            private yoyakuListSerivce: api.service.YoyakuListService
        ) {
            console.log("init controller");
            this.loadMaster();
        }

        public static get state() {
            return {
                url: TopController.URL,
                templateUrl: TopController.TEMPLATE_URL,
                controller: TopController,
                controllerAs: "ctl"
            };
        }

        public loadMaster(): void {
            this.loadKarteKubunList();
            this.loadJhokyoList();
            this.loadDobutsushuList();
            this.loadShinryoukaList();
            this.loadTantoiList();
            this.loadShujiiList();
        }

        private listSelect(selectedItems: api.vo.ItemVo[], row: uiGrid.IGridRow) {
            if (row.isSelected) {
                selectedItems.push(row.entity);
            } else {
                for (let i: number = 0; i < selectedItems.length; i++) {
                    if (selectedItems[i].id == row.entity['id']) {
                        selectedItems.splice(i, 1);
                        break;
                    }
                }
            }
        }

        private loadKarteKubunList(): void {
            let parent = this;
            this.yoyakuListSerivce.getKarteKubunList().then(
                function (response) {
                    parent.karteKubunGridOptions.data = response;
                    parent.karteKubunGridOptions.onRegisterApi = function (gridApi: uiGrid.IGridApi) {
                        gridApi.selection.on.rowSelectionChanged(parent.$scope, function (row) {
                            parent.listSelect(parent.selectedKarteKubunList, row);
                        });
                    }
                }
            );
        }

        private loadJhokyoList(): void {
            let parent = this;
            this.yoyakuListSerivce.getJhokyoList().then(
                function (response) {
                    parent.jhokyoGridOptions.data = response;
                    parent.jhokyoGridOptions.onRegisterApi = function (gridApi: uiGrid.IGridApi) {
                        gridApi.selection.on.rowSelectionChanged(parent.$scope, function (row) {
                            parent.listSelect(parent.selectedJhokyoList, row);
                        });
                    }
                }
            );
        }

        private loadDobutsushuList(): void {
            let parent = this;
            this.yoyakuListSerivce.getDobutsushuList().then(
                function (response) {
                    parent.dobutsushuGridOptions.data = response;
                    parent.dobutsushuGridOptions.onRegisterApi = function (gridApi: uiGrid.IGridApi) {
                        gridApi.selection.on.rowSelectionChanged(parent.$scope, function (row) {
                            parent.listSelect(parent.selectedDobutsushuList, row);
                        });
                    }
                }
            );
        }

        private loadShinryoukaList(): void {
            let parent = this;
            this.yoyakuListSerivce.getShinryoukaList().then(
                function (response) {
                    parent.shinryokaGridOptions.data = response;
                    parent.shinryokaGridOptions.onRegisterApi = function (gridApi: uiGrid.IGridApi) {
                        gridApi.selection.on.rowSelectionChanged(parent.$scope, function (row) {
                            parent.listSelect(parent.selectedShinryokaList, row);
                        });
                    }
                }
            );
        }


        private loadTantoiList(): void {
            let parent = this;
            this.yoyakuListSerivce.getTantoiList().then(
                function (response) {
                    parent.tantoiGridOptions.data = response;
                    parent.tantoiGridOptions.onRegisterApi = function (gridApi: uiGrid.IGridApi) {
                        gridApi.selection.on.rowSelectionChanged(parent.$scope, function (row) {
                            parent.listSelect(parent.selectedTantoiList, row);
                        });
                    }
                }
            );
        }


        private loadShujiiList(): void {
            let parent = this;
            this.yoyakuListSerivce.getShujiiList().then(
                function (response) {
                    parent.shujiiGridOptions.data = response;
                    parent.shujiiGridOptions.onRegisterApi = function (gridApi: uiGrid.IGridApi) {
                        gridApi.selection.on.rowSelectionChanged(parent.$scope, function (row) {
                            parent.listSelect(parent.selectedShujiiList, row);
                        });
                    }
                }
            );
        }

        public search(): void {
            console.log(this.selectedKarteKubunList);
            console.log(this.selectedJhokyoList);
            console.log(this.selectedDobutsushuList);
            console.log(this.selectedShinryokaList);
            console.log(this.selectedTantoiList);
            console.log(this.selectedShujiiList);
        }

    }
}

