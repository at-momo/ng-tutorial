
namespace ivet.common.menu.controller {

    export class AppointmentController {

        static STATE_ID: string = "appointment";
        static CONTROLLER_ID: string = "ivet.common.menu.appointment_controller";
        static URL: string = "/appointment/";
        static TEMPLATE_URL: string = "./assets/view/common/menu/appointment.html";

        public static $inject = [
            '$scope',
            '$state'
        ];


        constructor(
            private $scope: ng.IScope,
            private $state: ng.ui.IStateService) {
        }

        public static get state() {
            return {
                url: AppointmentController.URL,
                templateUrl: AppointmentController.TEMPLATE_URL,
                controller: AppointmentController,
                controllerAs: "ctl"
            };
        }

        public goClinic() {
            this.$state.go(appointment.clinic.controller.TopController.STATE_ID);
        }

    }
}

