
namespace ivet.common.menu.controller {

    export class TopController {

        static STATE_ID: string = "top";
        static CONTROLLER_ID: string = "ivet.common.menu.top_controller";
        static URL: string = "/index.html";
        static TEMPLATE_URL: string = "./assets/view/common/menu/top.html";

        constructor(private $scope: ng.IScope) {
            console.log("init controller");
        }

        public static get state() {
            return {
                url: TopController.URL,
                templateUrl: TopController.TEMPLATE_URL,
                controller: TopController,
                controllerAs: "ctl"
            };
        }

    }
}

