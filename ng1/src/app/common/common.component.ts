/// <reference path="../../references.d.ts" />

namespace ivet.component {
    
    export class CommonComponent extends foundation.Component {

        static COMPONENT_ID: string = "ivet.common";

        protected registRouter(): void {

            this.addRouter(
                ivet.common.menu.controller.TopController.STATE_ID,
                ivet.common.menu.controller.TopController.state
            );

            this.addRouter(
                ivet.common.menu.controller.AppointmentController.STATE_ID,
                ivet.common.menu.controller.AppointmentController.state
            );

        }

    }
}
