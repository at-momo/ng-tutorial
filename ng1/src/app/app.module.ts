/// <reference path="./../references.d.ts" />

namespace ivet.app {

    export class AppModule {
        public static instance: AppModule = null;
        private module: ng.IModule;
        static MODULE_ID: string = "ivet.app";

        constructor() {

            // 外部モジュールの初期化
            ivet.api.ApiModule.startup();

            this.module = angular.module(AppModule.MODULE_ID, [
                ivet.api.ApiModule.MODULE_ID,
                'ngResource', 'ui.router', 'ui.grid', 'ui.grid.cellNav', 'ui.grid.pinning', 'ui.grid.selection'])

            console.log(this.module);
            this.module.config([function () { console.log("setting config"); }]);
            this.registComponent();
            this.initPageLoad();
        }

        private registComponent() {
            console.log("Regist Component.");
            // commonComponentを登録
            new ivet.component.CommonComponent(this.module);
            new ivet.component.AppointmemtComponent(this.module);
        }

        private initPageLoad() {
            this.module.config(["$urlRouterProvider",
                function ($urlRouterProvider: ng.ui.IUrlRouterProvider) {
                    console.log("Init Config");
                    $urlRouterProvider.otherwise("/index.html");
                }]
            );
        }

        public static startup(): void {
            AppModule.getInstance();
        }

        public static getInstance(): AppModule {
            if (AppModule.instance == null) {
                AppModule.instance = new AppModule();
            }
            return AppModule.instance;
        }

    }
}
