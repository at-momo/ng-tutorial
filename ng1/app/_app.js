var foundation;
(function (foundation) {
    class Module {
        constructor(module) {
            this.module = module;
            this.registComponent();
        }
        static startup() {
            Module.getInstance();
        }
        static getInstance() {
            if (Module.instance == null) {
                Module.instance = Module.newInstance();
            }
            return Module.instance;
        }
        static newInstance() {
            return null;
        }
        registComponent() {
        }
    }
    Module.instance = null;
    foundation.Module = Module;
})(foundation || (foundation = {}));
var foundation;
(function (foundation) {
    class Component {
        constructor(module) {
            this.module = null;
            this.module = module;
            this.registController();
            this.registRouter();
        }
        registRouter() {
        }
        registController() {
        }
        addRouter(stateId, config) {
            this.module.config([
                '$stateProvider',
                function ($stateProvider) {
                    $stateProvider.state(stateId, config);
                }
            ]);
        }
    }
    foundation.Component = Component;
})(foundation || (foundation = {}));
var foundation;
(function (foundation) {
    class Controller {
    }
    foundation.Controller = Controller;
})(foundation || (foundation = {}));
var foundation;
(function (foundation) {
    class Service {
        constructor() {
        }
    }
    foundation.Service = Service;
})(foundation || (foundation = {}));
var ivet;
(function (ivet) {
    var api;
    (function (api) {
        class ApiModule {
            constructor() {
                console.log("Regist Api Module!");
                this.module = angular.module(ApiModule.MODULE_ID, ['ngResource']);
                this.registService();
            }
            registComponent() {
            }
            registService() {
                console.log("Regist Service!");
                this.module.service(api.service.YoyakuListService.SERVICE_ID, api.service.YoyakuListService);
            }
            static newInstance() {
                return new ApiModule();
            }
            static startup() {
                ApiModule.getInstance();
            }
            static getInstance() {
                if (ApiModule.instance == null) {
                    ApiModule.instance = ApiModule.newInstance();
                }
                return ApiModule.instance;
            }
        }
        ApiModule.instance = null;
        ApiModule.MODULE_ID = "ivet.api";
        api.ApiModule = ApiModule;
    })(api = ivet.api || (ivet.api = {}));
})(ivet || (ivet = {}));
var ivet;
(function (ivet) {
    var api;
    (function (api) {
        var service;
        (function (service) {
            class YoyakuListService {
                constructor($resource, $q) {
                    this.$resource = $resource;
                    this.$q = $q;
                }
                load() {
                    let parent = this;
                    let def = this.$q.defer();
                    if (this.yoyakuList != null) {
                        def.resolve(this.yoyakuList);
                    }
                    else {
                        let resoruce = this.$resource(YoyakuListService.END_POINT, { callback: 'JSON_CALLBACK' }, { get: { method: 'JSONP' } });
                        resoruce.get({ tgt: 'Qry' }).$promise.then(function (response) {
                            parent.yoyakuList = response;
                            def.resolve(parent.yoyakuList);
                        });
                    }
                    return def.promise;
                }
                getKarteKubunList() {
                    let def = this.$q.defer();
                    this.load().then(function (response) {
                        def.resolve(response.karteKubunList);
                    });
                    return def.promise;
                }
                getJhokyoList() {
                    let def = this.$q.defer();
                    this.load().then(function (response) {
                        def.resolve(response.jhokyoList);
                    });
                    return def.promise;
                }
                getDobutsushuList() {
                    let def = this.$q.defer();
                    this.load().then(function (response) {
                        def.resolve(response.dobutsushuList);
                    });
                    return def.promise;
                }
                getShinryoukaList() {
                    let def = this.$q.defer();
                    this.load().then(function (response) {
                        console.log(response.shinryoukaList);
                        def.resolve(response.shinryoukaList);
                    });
                    return def.promise;
                }
                getTantoiList() {
                    let def = this.$q.defer();
                    this.load().then(function (response) {
                        def.resolve(response.tantoiList);
                    });
                    return def.promise;
                }
                getShujiiList() {
                    let def = this.$q.defer();
                    this.load().then(function (response) {
                        def.resolve(response.shujiiList);
                    });
                    return def.promise;
                }
            }
            YoyakuListService.SERVICE_ID = "ivet.api.yoyaku_list_service";
            YoyakuListService.END_POINT = "http://218.219.157.229/yoyakuList";
            service.YoyakuListService = YoyakuListService;
        })(service = api.service || (api.service = {}));
    })(api = ivet.api || (ivet.api = {}));
})(ivet || (ivet = {}));
var ivet;
(function (ivet) {
    var api;
    (function (api) {
        var vo;
        (function (vo) {
            class ItemVo {
            }
            vo.ItemVo = ItemVo;
        })(vo = api.vo || (api.vo = {}));
    })(api = ivet.api || (ivet.api = {}));
})(ivet || (ivet = {}));
var ivet;
(function (ivet) {
    var api;
    (function (api) {
        var vo;
        (function (vo) {
            class YoyakuListVo {
            }
            vo.YoyakuListVo = YoyakuListVo;
        })(vo = api.vo || (api.vo = {}));
    })(api = ivet.api || (ivet.api = {}));
})(ivet || (ivet = {}));
var ivet;
(function (ivet) {
    var component;
    (function (component) {
        class CommonComponent extends foundation.Component {
            registRouter() {
                this.addRouter(ivet.common.menu.controller.TopController.STATE_ID, ivet.common.menu.controller.TopController.state);
                this.addRouter(ivet.common.menu.controller.AppointmentController.STATE_ID, ivet.common.menu.controller.AppointmentController.state);
            }
        }
        CommonComponent.COMPONENT_ID = "ivet.common";
        component.CommonComponent = CommonComponent;
    })(component = ivet.component || (ivet.component = {}));
})(ivet || (ivet = {}));
var ivet;
(function (ivet) {
    var component;
    (function (component) {
        class AppointmemtComponent extends foundation.Component {
            registRouter() {
                this.addRouter(ivet.appointment.clinic.controller.TopController.STATE_ID, ivet.appointment.clinic.controller.TopController.state);
            }
        }
        AppointmemtComponent.COMPONENT_ID = "ivet.appointment";
        component.AppointmemtComponent = AppointmemtComponent;
    })(component = ivet.component || (ivet.component = {}));
})(ivet || (ivet = {}));
var ivet;
(function (ivet) {
    var common;
    (function (common) {
        var menu;
        (function (menu) {
            var controller;
            (function (controller) {
                class TopController {
                    constructor($scope) {
                        this.$scope = $scope;
                        console.log("init controller");
                    }
                    static get state() {
                        return {
                            url: TopController.URL,
                            templateUrl: TopController.TEMPLATE_URL,
                            controller: TopController,
                            controllerAs: "ctl"
                        };
                    }
                }
                TopController.STATE_ID = "top";
                TopController.CONTROLLER_ID = "ivet.common.menu.top_controller";
                TopController.URL = "/index.html";
                TopController.TEMPLATE_URL = "./assets/view/common/menu/top.html";
                controller.TopController = TopController;
            })(controller = menu.controller || (menu.controller = {}));
        })(menu = common.menu || (common.menu = {}));
    })(common = ivet.common || (ivet.common = {}));
})(ivet || (ivet = {}));
var ivet;
(function (ivet) {
    var common;
    (function (common) {
        var menu;
        (function (menu) {
            var controller;
            (function (controller) {
                class AppointmentController {
                    constructor($scope, $state) {
                        this.$scope = $scope;
                        this.$state = $state;
                    }
                    static get state() {
                        return {
                            url: AppointmentController.URL,
                            templateUrl: AppointmentController.TEMPLATE_URL,
                            controller: AppointmentController,
                            controllerAs: "ctl"
                        };
                    }
                    goClinic() {
                        this.$state.go(ivet.appointment.clinic.controller.TopController.STATE_ID);
                    }
                }
                AppointmentController.STATE_ID = "appointment";
                AppointmentController.CONTROLLER_ID = "ivet.common.menu.appointment_controller";
                AppointmentController.URL = "/appointment/";
                AppointmentController.TEMPLATE_URL = "./assets/view/common/menu/appointment.html";
                AppointmentController.$inject = [
                    '$scope',
                    '$state'
                ];
                controller.AppointmentController = AppointmentController;
            })(controller = menu.controller || (menu.controller = {}));
        })(menu = common.menu || (common.menu = {}));
    })(common = ivet.common || (ivet.common = {}));
})(ivet || (ivet = {}));
var ivet;
(function (ivet) {
    var appointment;
    (function (appointment) {
        var clinic;
        (function (clinic) {
            var controller;
            (function (controller) {
                class TopController {
                    constructor($scope, yoyakuListSerivce) {
                        this.$scope = $scope;
                        this.yoyakuListSerivce = yoyakuListSerivce;
                        this.appointmentGridOptions = {
                            enableRowSelection: true,
                            enableRowHeaderSelection: false,
                            selectedItems: this.selectedKarteKubunList,
                            multiSelect: true,
                            columnDefs: [
                                { field: 'id', displayName: 'id', width: '100', visible: false },
                                { field: 'name', displayName: '予約日', width: '148' },
                                { field: 'name', displayName: '時刻', width: '148' },
                                { field: 'name', displayName: '飼主番号', width: '148' },
                                { field: 'name', displayName: '動物番号', width: '148' },
                                { field: 'name', displayName: '動物名', width: '148' },
                            ]
                        };
                        this.selectedKarteKubunList = [];
                        this.karteKubunGridOptions = {
                            enableRowSelection: true,
                            enableRowHeaderSelection: false,
                            selectedItems: this.selectedKarteKubunList,
                            multiSelect: true,
                            columnDefs: [
                                { field: 'id', displayName: 'id', width: '100', visible: false },
                                { field: 'name', displayName: "カルテ区分", width: '148' }
                            ]
                        };
                        this.selectedJhokyoList = [];
                        this.jhokyoGridOptions = {
                            enableRowSelection: true,
                            enableRowHeaderSelection: false,
                            selectedItems: this.selectedJhokyoList,
                            multiSelect: true,
                            columnDefs: [
                                { field: 'id', displayName: 'id', width: '100', visible: false },
                                { field: 'name', displayName: "状況", width: '148' }
                            ]
                        };
                        this.selectedDobutsushuList = [];
                        this.dobutsushuGridOptions = {
                            enableRowSelection: true,
                            enableRowHeaderSelection: false,
                            selectedItems: this.selectedDobutsushuList,
                            multiSelect: true,
                            columnDefs: [
                                { field: 'id', displayName: 'id', width: '100', visible: false },
                                { field: 'name', displayName: "動物種", width: '148' }
                            ]
                        };
                        this.selectedShinryokaList = [];
                        this.shinryokaGridOptions = {
                            enableRowSelection: true,
                            enableRowHeaderSelection: false,
                            selectedItems: this.selectedShinryokaList,
                            multiSelect: true,
                            columnDefs: [
                                { field: 'id', displayName: 'id', width: '100', visible: false },
                                { field: 'name', displayName: "診療科", width: '148' }
                            ]
                        };
                        this.selectedTantoiList = [];
                        this.tantoiGridOptions = {
                            enableRowSelection: true,
                            enableRowHeaderSelection: false,
                            selectedItems: this.selectedTantoiList,
                            multiSelect: true,
                            columnDefs: [
                                { field: 'id', displayName: 'id', width: '100', visible: false },
                                { field: 'name', displayName: "担当医", width: '148' }
                            ]
                        };
                        this.selectedShujiiList = [];
                        this.shujiiGridOptions = {
                            enableRowSelection: true,
                            enableRowHeaderSelection: false,
                            selectedItems: this.selectedShujiiList,
                            multiSelect: true,
                            columnDefs: [
                                { field: 'id', displayName: 'id', width: '100', visible: false },
                                { field: 'name', displayName: "担当医", width: '148' }
                            ]
                        };
                        console.log("init controller");
                        this.loadMaster();
                    }
                    static get state() {
                        return {
                            url: TopController.URL,
                            templateUrl: TopController.TEMPLATE_URL,
                            controller: TopController,
                            controllerAs: "ctl"
                        };
                    }
                    loadMaster() {
                        this.loadKarteKubunList();
                        this.loadJhokyoList();
                        this.loadDobutsushuList();
                        this.loadShinryoukaList();
                        this.loadTantoiList();
                        this.loadShujiiList();
                    }
                    listSelect(selectedItems, row) {
                        if (row.isSelected) {
                            selectedItems.push(row.entity);
                        }
                        else {
                            for (let i = 0; i < selectedItems.length; i++) {
                                if (selectedItems[i].id == row.entity['id']) {
                                    selectedItems.splice(i, 1);
                                    break;
                                }
                            }
                        }
                    }
                    loadKarteKubunList() {
                        let parent = this;
                        this.yoyakuListSerivce.getKarteKubunList().then(function (response) {
                            parent.karteKubunGridOptions.data = response;
                            parent.karteKubunGridOptions.onRegisterApi = function (gridApi) {
                                gridApi.selection.on.rowSelectionChanged(parent.$scope, function (row) {
                                    parent.listSelect(parent.selectedKarteKubunList, row);
                                });
                            };
                        });
                    }
                    loadJhokyoList() {
                        let parent = this;
                        this.yoyakuListSerivce.getJhokyoList().then(function (response) {
                            parent.jhokyoGridOptions.data = response;
                            parent.jhokyoGridOptions.onRegisterApi = function (gridApi) {
                                gridApi.selection.on.rowSelectionChanged(parent.$scope, function (row) {
                                    parent.listSelect(parent.selectedJhokyoList, row);
                                });
                            };
                        });
                    }
                    loadDobutsushuList() {
                        let parent = this;
                        this.yoyakuListSerivce.getDobutsushuList().then(function (response) {
                            parent.dobutsushuGridOptions.data = response;
                            parent.dobutsushuGridOptions.onRegisterApi = function (gridApi) {
                                gridApi.selection.on.rowSelectionChanged(parent.$scope, function (row) {
                                    parent.listSelect(parent.selectedDobutsushuList, row);
                                });
                            };
                        });
                    }
                    loadShinryoukaList() {
                        let parent = this;
                        this.yoyakuListSerivce.getShinryoukaList().then(function (response) {
                            parent.shinryokaGridOptions.data = response;
                            parent.shinryokaGridOptions.onRegisterApi = function (gridApi) {
                                gridApi.selection.on.rowSelectionChanged(parent.$scope, function (row) {
                                    parent.listSelect(parent.selectedShinryokaList, row);
                                });
                            };
                        });
                    }
                    loadTantoiList() {
                        let parent = this;
                        this.yoyakuListSerivce.getTantoiList().then(function (response) {
                            parent.tantoiGridOptions.data = response;
                            parent.tantoiGridOptions.onRegisterApi = function (gridApi) {
                                gridApi.selection.on.rowSelectionChanged(parent.$scope, function (row) {
                                    parent.listSelect(parent.selectedTantoiList, row);
                                });
                            };
                        });
                    }
                    loadShujiiList() {
                        let parent = this;
                        this.yoyakuListSerivce.getShujiiList().then(function (response) {
                            parent.shujiiGridOptions.data = response;
                            parent.shujiiGridOptions.onRegisterApi = function (gridApi) {
                                gridApi.selection.on.rowSelectionChanged(parent.$scope, function (row) {
                                    parent.listSelect(parent.selectedShujiiList, row);
                                });
                            };
                        });
                    }
                    search() {
                        console.log(this.selectedKarteKubunList);
                        console.log(this.selectedJhokyoList);
                        console.log(this.selectedDobutsushuList);
                        console.log(this.selectedShinryokaList);
                        console.log(this.selectedTantoiList);
                        console.log(this.selectedShujiiList);
                    }
                }
                TopController.CONTROLLER_ID = "ivet.appointment.clinic.top_controller";
                TopController.STATE_ID = "appointment.clinic";
                TopController.URL = "clinic/";
                TopController.TEMPLATE_URL = "./assets/view/appointment/clinic/top.html";
                TopController.$inject = [
                    '$scope',
                    ivet.api.service.YoyakuListService.SERVICE_ID
                ];
                controller.TopController = TopController;
            })(controller = clinic.controller || (clinic.controller = {}));
        })(clinic = appointment.clinic || (appointment.clinic = {}));
    })(appointment = ivet.appointment || (ivet.appointment = {}));
})(ivet || (ivet = {}));
var ivet;
(function (ivet) {
    var app;
    (function (app) {
        class AppModule {
            constructor() {
                ivet.api.ApiModule.startup();
                this.module = angular.module(AppModule.MODULE_ID, [
                    ivet.api.ApiModule.MODULE_ID,
                    'ngResource', 'ui.router', 'ui.grid', 'ui.grid.cellNav', 'ui.grid.pinning', 'ui.grid.selection']);
                console.log(this.module);
                this.module.config([function () { console.log("setting config"); }]);
                this.registComponent();
                this.initPageLoad();
            }
            registComponent() {
                console.log("Regist Component.");
                new ivet.component.CommonComponent(this.module);
                new ivet.component.AppointmemtComponent(this.module);
            }
            initPageLoad() {
                this.module.config(["$urlRouterProvider",
                    function ($urlRouterProvider) {
                        console.log("Init Config");
                        $urlRouterProvider.otherwise("/index.html");
                    }]);
            }
            static startup() {
                AppModule.getInstance();
            }
            static getInstance() {
                if (AppModule.instance == null) {
                    AppModule.instance = new AppModule();
                }
                return AppModule.instance;
            }
        }
        AppModule.instance = null;
        AppModule.MODULE_ID = "ivet.app";
        app.AppModule = AppModule;
    })(app = ivet.app || (ivet.app = {}));
})(ivet || (ivet = {}));
var ivet;
(function (ivet) {
    ivet.app.AppModule.startup();
})(ivet || (ivet = {}));
//# sourceMappingURL=_app.js.map