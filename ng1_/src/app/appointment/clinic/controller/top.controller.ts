

namespace ivet.appointment.clinic.controller {

    export class TopController {

        static CONTROLLER_ID: string = "ivet.appointment.clinic.top_controller";
        static STATE_ID: string = "appointment.clinic";
        static URL: string = "clinic/";
        static TEMPLATE_URL: string = "./assets/view/appointment/clinic/top.html";

        public tantoiGridOptions: any = {};
        public shujiiGridOptions: any = {};

        public static $inject = [
            api.service.YoyakuListService.SERVICE_ID
        ];

        constructor(
            private $scope: ng.IScope,
            private yoyakuListSerivce: api.service.YoyakuListService
        ) {
            console.log("init controller");
            this.loadMaster();
        }

        public static get state() {
            return {
                url: TopController.URL,
                templateUrl: TopController.TEMPLATE_URL,
                controller: TopController,
                controllerAs: "ctl"
            };
        }

        public loadMaster(): void {
            this.loadTantoiList();
            this.loadShujiiList();
        }

        private onClicked($scope: ng.IScope, gridApi: uiGrid.IGridApi) {
            /*
            scope.currentSelctedrows = gridApi.selection.getSelectedRows();
            scope.countRows = gridApi.selection.getSelectedRows().length;
            scope.$parent.kartekubunGrdSelId = [];//クリア
            var currentSelectedRows = scope.currentSelctedrows;//「選択されている行」を抽出
            var looplimit = scope.countRows;
            $parent.kartekubunGrdSelId = [];//初期化
            for (var i = 0; i < looplimit; i++) {
                scope.$parent.kartekubunGrdSelId[i] = currentSelectedRows[i].name;
            }
            */

            //$log.log(scope.$parent.kartekubunGrdSelId.toString());

            //上位コントローラに行選択を通知
            //scope.$emit('CellSelected', scope.ivtGridName);
            //formCtlr.elemtRregist(scope.ivtGridName);
        }


        private loadTantoiList(): void {
            let parent = this;
            this.yoyakuListSerivce.getTantoiList().then(
                function (response) {
                    parent.tantoiGridOptions = {
                        data: response,
                        enableRowSelection: true,
                        enableRowHeaderSelection: false,
                        multiSelect: true,
                        columnDefs: [
                            { field: 'id', displayName: 'id', width: '100', visible: false },
                            { field: 'name', displayName: "担当医", width: '148' }
                        ],
                        onRegisterApi: function (gridApi: uiGrid.IGridApi) {
                            gridApi.selection.on.rowSelectionChanged(parent.$scope, function (row) {
                                parent.onClicked(parent.$scope, gridApi);
                            });
                            gridApi.selection.on.rowSelectionChangedBatch(parent.$scope, function (row) {
                                parent.onClicked(parent.$scope, gridApi);
                            });
                        }
                    };
                }
            );
        }


        private loadShujiiList(): void {
            let parent = this;
            this.yoyakuListSerivce.getShujiiList().then(
                function (response) {
                    parent.shujiiGridOptions = {
                        data: response,
                        enableRowSelection: true,
                        enableRowHeaderSelection: false,
                        multiSelect: true,
                        columnDefs: [
                            { field: 'id', displayName: 'id', width: '100', visible: false },
                            { field: 'name', displayName: "主治医", width: '148' }
                        ]
                    };
                }
            );
        }

    }
}

